# Base the image on Ubuntu 14.04 LTS
FROM ubuntu:trusty

# Install required packages
RUN apt-get update \
 && apt-get install -y rsyslog python-setuptools python-pip curl \
 && apt-get clean

# Download the AWS CloudWatch log agent script
RUN curl https://s3.amazonaws.com/aws-cloudwatch/downloads/latest/awslogs-agent-setup.py -o awslogs-agent-setup.py

# Install and configure the CloudWatch agent
COPY ./etc/awslogs.conf /etc/awslogs.conf
RUN python ./awslogs-agent-setup.py -n -r us-east-1 -c /etc/awslogs.conf

# Install and configure a process control system
RUN pip install supervisor
COPY ./etc/supervisord.conf /etc/supervisord.conf

# Copy rsyslog config files into the image
COPY ./etc/rsyslog.conf /etc/rsyslog.conf
COPY ./etc/rsyslog.d/* /etc/rsyslog.d/
RUN sed -i "s/authpriv.none/authpriv.none,local6.none,local7.none/" /etc/rsyslog.d/50-default.conf

# Expose rsyslog ports
EXPOSE 514/tcp 514/udp

# Start rsyslog and the CloudWatch agent with supervisor
CMD ["/usr/local/bin/supervisord", "-c", "/etc/supervisord.conf"]
