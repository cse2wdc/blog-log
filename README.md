# rsyslog + AWS log agent

This Docker image is designed to accept log messages on port 514 and upload
them to AWS CloudWatch.

At the moment messages are expected for "api", "frontend" and "nginx".
This behaviour can be customised by editing awslogs.conf and rsyslog.d/*.conf
to suit your situation before building the image.

## Linked containers

```bash
docker build -t rsyslog-aws .
docker run --rm --name=log rsyslog-aws
```

```bash
docker run -it --rm --link log debian:jessie \
  sh -c "echo 'A log entry' | logger -t frontend -p local6.info -n log -P 514"
```

Here's a handy tip. You can separate stdout and stderr into different error
logs using this funky bash syntax.

```bash
my-frontend-server \
  >  >(logger -t frontend -p local6.info -n log -P 514) \
  2> >(logger -t frontend -p local7.info -n log -P 514)
```
